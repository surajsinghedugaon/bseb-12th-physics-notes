import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lottie/lottie.dart';

import 'login_page.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  State<ForgotPassword> createState() => _LoginPage1State();
}

//Login Page 1 code start
class _LoginPage1State extends State<ForgotPassword> {
  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.selected,
      MaterialState.focused,
      MaterialState.pressed,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.white;
    }
    return Colors.white;
  }

  bool? isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: ListView(
        children: [
          Container(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 100),
                child: Text(
                  'Forgot Password?',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Lottie.network(
                'https://assets2.lottiefiles.com/packages/lf20_rsjmkul3.json',
                height: 240,
                width: 300),
          ),
          Container(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Text(
                  textAlign: TextAlign.center,
                  'Enter the email address associated with your account.',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, top: 20),
                child: Text(
                  textAlign: TextAlign.center,
                  'We will email you a link to reset your password.',
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.white70,
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 35, right: 35, top: 20),
              child: SizedBox(
                height: 65,
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  color: Colors.blueAccent,
                  shadowColor: Colors.blueAccent,
                  elevation: 10,
                  child: const TextField(
                    keyboardType: TextInputType.emailAddress,
                    cursorColor: Colors.white,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.mail),
                        hintText: "Enter your registered email",
                        border: InputBorder.none,
                        iconColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(vertical: 18),
                        hintStyle:
                            TextStyle(color: Colors.white70, fontSize: 18),
                        // prefixIcon: Icon(Icons.person,),
                        prefixIconColor: Colors.white),
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 90, right: 90, top: 30),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      elevation: 10,
                      padding: EdgeInsets.symmetric(
                          horizontal: 40.0, vertical: 20.0),
                      primary: Colors.white,
                      shape: StadiumBorder()),
                  onPressed: () {
                    Fluttertoast.showToast(
                        msg:
                            'We will sent you link on your email please check it for reset password',
                        toastLength: Toast.LENGTH_SHORT,
                        backgroundColor: Colors.black);
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LoginPage(),
                        ));
                    // Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPasswordVerification(),));
                  },
                  child: Text(
                    'Send',
                    style: TextStyle(color: Colors.blueAccent, fontSize: 20),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
//forgot password code end

//
// class ForgotPasswordVerification extends StatefulWidget {
//   const ForgotPasswordVerification({Key? key}) : super(key: key);
//
//   @override
//   State<ForgotPasswordVerification> createState() => _LoginPage2State();
// }
//
// //Login Page 1 code start
// class _LoginPage2State extends State<ForgotPasswordVerification> {
//   Color getColor(Set<MaterialState> states) {
//     const Set<MaterialState> interactiveStates = <MaterialState>{
//       MaterialState.selected,
//       MaterialState.focused,
//       MaterialState.pressed,
//     };
//     if (states.any(interactiveStates.contains)) {
//       return Colors.white;
//     }
//     return Colors.white;
//   }
//
//   bool? isChecked = false;
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.blueAccent,
//       body: ListView(
//         children: [
//           Container(
//             child: Center(
//               child: Padding(
//                 padding: const EdgeInsets.only(top: 100),
//                 child: Text(
//                   'Verification',
//                   style: TextStyle(
//                     fontSize: 25,
//                     fontWeight: FontWeight.bold,
//                     color: Colors.white,
//                   ),
//                 ),
//               ),
//             ),
//           ),
//
//           Container(
//             child: Lottie.network('https://assets2.lottiefiles.com/packages/lf20_qVyybKaLrI.json',height: 240,width: 300),
//           ),
//
//           Container(
//             child: Center(
//               child: Padding(
//                 padding: const EdgeInsets.only(left: 15, right: 15),
//                 child: Text(
//                   textAlign: TextAlign.center,
//                   'Enter the verification code we just sent you on you email address.',
//                   style: TextStyle(
//                     fontSize: 20,
//                     fontWeight: FontWeight.bold,
//                     color: Colors.white,
//                   ),
//                 ),
//               ),
//             ),
//           ),
//
//           Container(
//             child: Padding(
//               padding: const EdgeInsets.only(left: 35, right: 35, top: 20),
//               child: SizedBox(
//                 height: 65,
//                 child: Card(
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(10)),
//                   color: Colors.blueAccent,
//                   shadowColor: Colors.blueAccent,
//                   elevation: 10,
//                   child: const TextField(
//                     keyboardType: TextInputType.emailAddress,
//                     cursorColor: Colors.white,
//                     style: TextStyle(color: Colors.white),
//                     decoration: InputDecoration(
//                         prefixIcon: Icon(Icons.mail),
//                         hintText: "Enter your registered email",
//                         border: InputBorder.none,
//                         iconColor: Colors.white,
//                         contentPadding: EdgeInsets.symmetric(vertical: 18),
//                         hintStyle:
//                         TextStyle(color: Colors.white70, fontSize: 18),
//                         // prefixIcon: Icon(Icons.person,),
//                         prefixIconColor: Colors.white),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//
//           Container(
//             child: Center(
//               child: Padding(
//                 padding: const EdgeInsets.only(left: 15, right: 15),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   children: [
//                     Text(
//                       textAlign: TextAlign.center,
//                       "If you don't receive a code!.   ",
//                       style: TextStyle(
//                         fontSize: 14,
//                         color: Colors.white70,
//                       ),
//                     ),
//                     TextButton(onPressed: () {
//
//                     }, child: Text('Resend', style: TextStyle(color: Colors.orangeAccent, fontWeight: FontWeight.bold),))
//                   ],
//                 ),
//               ),
//             ),
//           ),
//
//
//           Container(
//             child: Padding(
//               padding: const EdgeInsets.only(left: 90, right: 90, top: 30),
//               child: ElevatedButton(
//                   style: ElevatedButton.styleFrom(
//                       elevation: 10,
//                       padding: EdgeInsets.symmetric(
//                           horizontal: 40.0, vertical: 20.0),
//                       primary: Colors.white,
//                       shape: StadiumBorder()),
//                   onPressed: () {
//
//                   },
//                   child: Text(
//                     'Send',
//                     style: TextStyle(color: Colors.blueAccent, fontSize: 20),
//                   )),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
// //forgot password code end
