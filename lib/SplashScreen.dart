
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import 'login_page.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  void initState() {
    super.initState();
    Timer(Duration(seconds: 5),()=>
                Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => LoginPage())
        )
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("BSEB 12th Class", style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),),
                Text('Physics Notes',style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),),
                Lottie.network(
                    'https://assets6.lottiefiles.com/packages/lf20_hylaaytn.json',width: 200, height: 200),
                CircularProgressIndicator(color: Colors.white,)
          ],
        ),
      ),
    );
  }
}
