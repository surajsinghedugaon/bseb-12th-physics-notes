import 'package:curved_labeled_navigation_bar/curved_navigation_bar.dart';
import 'package:curved_labeled_navigation_bar/curved_navigation_bar_item.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';



class HomePage extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<HomePage> {
int selectedPageIndex = 0;
var screens = [
  MaterialApp(color: Colors.orangeAccent,
  debugShowCheckedModeBanner: false,
  home: Scaffold(
    appBar: AppBar(
      title: Text('Home'),
    ),
  ),
  ),
  MaterialApp(color: Colors.orangeAccent,
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(
        title: Text('Notes'),
      ),
    ),
  ),
  MaterialApp(color: Colors.orangeAccent,
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(
        title: Text('Videos'),
      ),
    ),
  ),
  MaterialApp(color: Colors.orangeAccent,
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              SizedBox(
                height: 120,
                width: 120,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Lottie.network('https://assets7.lottiefiles.com/packages/lf20_NODCLWy3iX.json'))
              ),
              SizedBox(
                height: 10,
                child: Text('User Name', style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
              )
            ],
          ),
        ),
      ),
    ),
  )
];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'home',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: screens[selectedPageIndex],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: selectedPageIndex,
            items: [
              BottomNavigationBarItem(backgroundColor: Colors.blue,icon: Icon(Icons.home), label: 'Home'),
              BottomNavigationBarItem(backgroundColor: Colors.blue,icon: Icon(Icons.picture_as_pdf), label: 'Notes'),
              BottomNavigationBarItem(backgroundColor: Colors.blue,icon: Icon(Icons.video_file), label: 'Videos'),
              BottomNavigationBarItem(backgroundColor: Colors.blue,icon: Icon(Icons.person), label: 'Profile')
            ],
        onTap: (index){
            selectedPageIndex = index;
            setState(() {});
        },
        ),
      ),
    );
  }
}

// void _showDailog(BuildContext context){
//   showDialog(context: context, builder: (BuildContext context){
//     return AlertDialog(
//       title: Text('data'),
//     )
//   })
// }