import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:bseb_12th_physics_notes/login_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'SplashScreen.dart';
import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Splash(),
    );
  }
}



// backgroundColor: Colors.blueAccent,
// bottomNavigationBar: CurvedNavigationBar(
// backgroundColor: Colors.blueAccent,
// color: Colors.white,
// animationDuration: Duration(milliseconds: 300),
// onTap: (index){
//
// },
// items: [
//
// Icon(
//
// Icons.home,
//
// ),
// Icon(Icons.notifications,
//
// ),
// Icon(Icons.person,
// ),
// labe
// ],
//
// ),


